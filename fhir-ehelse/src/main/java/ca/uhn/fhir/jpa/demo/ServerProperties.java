package ca.uhn.fhir.jpa.demo;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by larol on 15.05.2017.
 */
public class ServerProperties {

    static Properties prop = null;

    static Properties getProperties() throws Exception {

        if (prop == null) {

            prop = new Properties();
            InputStream input = null;
            String filename = "fhir_server.properties";
            input = FhirServerConfigDstu3.class.getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                throw new SQLException("Unable to find fhir_server.properties");
            }

            //load a properties file from class path, inside static method
            try {
                prop.load(input);
            } catch (IOException e) {
                throw new Exception("Unable to load properties:" + e);
            }

        }
        return prop;
    }
}
