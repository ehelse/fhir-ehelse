# README #

This code is BSD licensed (and HAPI is Apache licensed). It comes with no warranties, and the security mechanisms have both known and unknown flaws, including performance issues. But it sort of works, so you can pick up the code and use and modify it under BSD. Have fun.

To build, run

mvn -DskipTests install

Then cd into hapi-fhir-ehelse-2 and run

mvn jetty:run

(For more permanent installation, use for example Tomcat)

This configuration requires an MS SQL database, running on localhost, according to the following connection string:

"jdbc:sqlserver://localhost:1433;databaseName=fhir;user=fhir;password=JP5YcLkE";

Note: Obviously you don't want to use a password that's on git, but you know that...

Note: This server has no security. Check OAuhtAuthorizationInterceptor for hint how to add security. Needs to be commented into the file FhirServerConfig also.